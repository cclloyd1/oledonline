import {create} from 'zustand'
import { persist, createJSONStorage } from 'zustand/middleware'

const useAppStore = create(
    persist(
        (set, get) => ({
            drawerOpen: true,
            settingsOpen: window.innerWidth > 768,
            controlDrawerOpen: true,
            liveUpdate: true,
            outlineCanvas: false,
            outputType: 'png',
            backgroundBrightness: 0,
            statSize: 8,
            mobileImageRef: null,
            previousHeight: null,
            useURL: false,
            rememberImage: false,
            setStatSize: (value) => set({statSize: value}),
            toggleUseURL: () => set({useURL: !get().useURL}),
            toggleSettingsOpen: () => set({settingsOpen: !get().settingsOpen}),
            toggleLiveUpdate: () => set({liveUpdate: !get().liveUpdate}),
            toggleOutlineCanvas: () => set({outlineCanvas: !get().outlineCanvas}),
            toggleRememberImage: () => set({rememberImage: !get().rememberImage}),
            setBackgroundBrightness: (value) => set({backgroundBrightness: value}),
            isMobile: window.innerWidth < 768,
            toggleDrawer: () => set(state => ({drawerOpen: !state.drawerOpen})),
            toggleControlDrawer: () => set(state => ({controlDrawerOpen: !state.controlDrawerOpen})),
            setOutputType: (value) => set(state => ({outputType: value})),
            setMobileImageRef: (value) => {
                if (get().mobileImageRef === null) set({mobileImageRef: value,})
                else if (get().previousHeight === null) set({
                    mobileImageRef: value,
                    previousHeight: get().mobileImageRef,
                })
                else set({
                        mobileImageRef: get().previousHeight,
                        previousHeight: get().mobileImageRef,
                    })

            }
        }),
        {
            name: 'oledonline-app', // name of the item in the storage (must be unique)
            storage: createJSONStorage(() => localStorage), // (optional) by default, 'localStorage' is used// partialize: (state) =>
            partialize: (state) =>
                Object.fromEntries(
                    Object.entries(state).filter(([key]) => [
                        'liveUpdate',
                        'outlineCanvas',
                        'outputType',
                        'backgroundBrightness',
                        'useURL',
                        'statSize',
                        'mobileImageRef',
                        'previousHeight',
                        'settingsOpen',
                        'rememberImage',
                    ].includes(key))
                ),

        }
    )
)

export default useAppStore;
