import {create} from 'zustand'
import * as AsBind from 'as-bind';
import {createJSONStorage, persist} from 'zustand/middleware';


const copyData = (src, dest) => {
    for (let i = 0; i < src.length; i++) dest[i] = src[i];
}

const useImageStore = create(
    persist(
        (set, get) => ({
            inputFile: null,
            inputURL: '',
            wasm: null,
            loadedWasm: false,
            loaded: false,
            canvas: null,
            img: null,
            tRed: 0,
            tGreen: 0,
            tBlue: 0,
            blackPercent: 0,
            black: null,
            pixelTotal: 0,
            width: 0,
            height: 0,
            manipulating: false,
            liveUpdate: true,
            slidersLocked: true,
            originalData: null,
            originalDataURL: null,
            currentData: null,
            redToggled: false,
            toggleRed: () => set({redToggled: !get().redToggled}),
            osc: null,
            setRed: (value) => set({tRed: value}),
            setOriginalDataURL: (value) => set({originalDataURL: value}),
            setGreen: (value) => set({tGreen: value}),
            setBlue: (value) => set({tBlue: value}),
            toggleSlidersLocked: () => set({slidersLocked: !get().slidersLocked}),
            setInputFile: (inputFile) => {
                const newImg = new Image();
                newImg.src = URL.createObjectURL(inputFile);
                newImg.onload = () => {
                    const osc = new OffscreenCanvas(newImg.width, newImg.height);
                    const oscCtx = osc.getContext('2d');
                    oscCtx.drawImage(newImg, 0, 0)
                    set({
                        inputFile: inputFile,
                        loaded: true,
                        width: newImg.width,
                        height: newImg.height,
                        osc: osc,
                        originalData: oscCtx.getImageData(0, 0, newImg.width, newImg.height),
                        currentData: oscCtx.getImageData(0, 0, newImg.width, newImg.height),
                    })
                }
            },
            setInputURL: (value) => set({inputURL: value}),
            loadImageURL: () => {
                const inputURL = get().inputURL;
                const newImg = new Image();
                newImg.src = inputURL;
                newImg.crossOrigin = "anonymous";
                newImg.onload = () => {
                    console.log('loading image from url', inputURL);
                    const osc = new OffscreenCanvas(newImg.width, newImg.height);
                    const oscCtx = osc.getContext('2d');
                    oscCtx.drawImage(newImg, 0, 0)
                    set({
                        loaded: true,
                        width: newImg.width,
                        height: newImg.height,
                        osc: osc,
                        originalData: oscCtx.getImageData(0, 0, newImg.width, newImg.height),
                        currentData: oscCtx.getImageData(0, 0, newImg.width, newImg.height),
                    })
                    get().loadWasm();
                }
            },
            setImg: (img) => set({ img: img }),
            setManipulating: (value) => set({ manipulating: value }),
            setCanvas: (canvas) => set({ canvas: canvas }),
            setLoaded: (value) => set({ loaded: value }),
            setOriginalData: (value) => set({ originalData: value }),
            setCurrentData: (value) => set({ currentData: value }),
            clearImage: () => set({
                img: null,
                inputFile: null,
                inputURL: '',
                loaded: false,
                wasm: null,
                loadedWasm: false,
                originalData: null,
                currentData: null,
                black: null,
                width: 0,
                height: 0,
            }),
            loadWasm: async () => {
                console.log('Loading wasm...')
                const arraySize = (get().width * get().height * 4) >>> 0;
                const nPages = ((arraySize + 0xffff) & ~0xffff) >>> 16;
                const importObject = {
                    module: {},
                    env: {
                        //memory: new WebAssembly.Memory({ initial: nPages })
                        memory: new WebAssembly.Memory({ initial: nPages })
                    }
                };
                const wasm = await fetch('oled-wasm.wasm');
                const instance = await AsBind.instantiate(wasm, importObject);
                set({wasm: instance, loadedWasm: true});
                console.log(`Reloaded wasm with ${nPages} memory pages`);
            },
            resetCanvas: async () => {
                const canvas = get().canvas.current;
                const originalData = get().originalData;
                const ctx = canvas.getContext('2d');
                ctx.putImageData(originalData, 0, 0);
            },
            runOledify: async () => {
                console.time('OLED_BLACKEN')
                const wasm = get().wasm;
                const canvas = get().canvas.current;
                const width = get().width;
                const height = get().height;
                const originalData = get().originalData;
                const currentData = get().currentData;
                const slidersLocked = get().slidersLocked;
                const tRed = get().tRed;
                const tGreen = get().tGreen;
                const tBlue = get().tBlue;

                if (originalData === null) return;

                const ctx = canvas.getContext('2d');
                const bytes = new Uint8ClampedArray(wasm.importObject.env.memory.buffer);

                copyData(originalData.data, bytes);
                wasm.exports.oledify(width, height, tRed, slidersLocked ? tRed : tGreen, slidersLocked ? tRed : tBlue);
                const modifiedData = currentData.data;
                for (let i = 0; i < modifiedData.length; i++) modifiedData[i] = bytes[i]
                ctx.putImageData(currentData, 0, 0);

                const black = wasm.exports.countBlack(width, height);

                set({
                    manipulating: false,
                    currentData: currentData,
                    black: black,
                    redToggled: false,
                });
                console.timeEnd('OLED_BLACKEN')
            },
            runRedify: async () => {
                // TODO: Red state is messed up a lot, needs fixing
                console.time('OLED_REDDEN')
                const wasm = get().wasm;
                const canvas = get().canvas.current;
                const width = get().width;
                const height = get().height;
                const originalData = get().originalData;
                const currentData = get().currentData;
                const slidersLocked = get().slidersLocked;
                const tRed = get().tRed;
                const tGreen = get().tGreen;
                const tBlue = get().tBlue;

                const ctx = canvas.getContext('2d');
                const bytes = new Uint8ClampedArray(wasm.importObject.env.memory.buffer);

                copyData(originalData.data, bytes);
                wasm.exports.redify(width, height, tRed, slidersLocked ? tRed : tGreen, slidersLocked ? tRed : tBlue);
                const modifiedData = currentData.data;
                for (let i = 0; i < modifiedData.length; i++) modifiedData[i] = bytes[i]
                ctx.putImageData(currentData, 0, 0);

                const black = wasm.exports.countRed(width, height);

                set({
                    manipulating: false,
                    currentData: currentData,
                    black: black,
                    redToggled: true,
                });
                console.timeEnd('OLED_REDDEN')
            },
            readBlack: async () => {
                console.time('OLED_READ_BLACK')
                const wasm = get().wasm;
                const width = get().width;
                const height = get().height;
                const originalData = get().originalData;
                const bytes = new Uint8ClampedArray(wasm.importObject.env.memory.buffer);

                if (!originalData) return;

                copyData(originalData.data, bytes);
                const black = wasm.exports.countBlack(width, height);

                set({black: black});
                console.timeEnd('OLED_READ_BLACK')
            },
        }),
        {
            name: 'oledonline-image', // name of the item in the storage (must be unique)
            storage: createJSONStorage(() => localStorage), // (optional) by default, 'localStorage' is used
            partialize: (state) =>
                Object.fromEntries(
                    Object.entries(state).filter(([key]) => [
                        //'inputFile',
                        'inputURL',
                        'tRed',
                        'tGreen',
                        'tBlue',
                        //'originalDataURL',
                        'width',
                        'height',
                    ].includes(key))
                ),

        }
    )
)

export default useImageStore;
