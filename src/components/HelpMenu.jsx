import React from 'react';
import HelpOutlineIcon from "@mui/icons-material/HelpOutline";
import HelpDialog from "./HelpDialog";
import {IconButton} from "@mui/material";


export default function HelpMenu() {
    const [open, setOpen] = React.useState(false);

    const toggleOpen = () => setOpen(!open);

    return (
        <>
            <IconButton size={'large'} onClick={toggleOpen} >
                <HelpOutlineIcon sx={{cursor: 'pointer'}}/>
            </IconButton>
            <HelpDialog open={open} onClose={toggleOpen}/>
        </>
    );
}
