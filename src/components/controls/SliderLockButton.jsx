import React from 'react';
import {styled} from '@mui/material/styles';
import LockIcon from '@mui/icons-material/Lock';
import LockOpenIcon from '@mui/icons-material/LockOpen';
import Box from "@mui/material/Box";
import grey from "@mui/material/colors/grey";
import Color from "color";
import useImageStore from "../../store/image.js";


const StyledBox = styled(Box)(({theme, locked}) => ({
    width: theme.spacing(2.5),
    height: `calc(50% - 12px)`,
    borderColor: grey[500],
    borderStyle: 'solid',
    borderWidth: 0,
    borderLeftWidth: 3,
    position: 'relative',
    left: '25%',
    [theme.breakpoints.down('md')]: {
        left: '12.5%',
    },
    ...(!locked && {
        borderLeftColor: `${Color(grey[500]).fade(0.5)}`,
        borderTopColor: 'transparent',
        borderBottomColor: 'transparent',
    }),
    ...(locked && {
        display: 'none',
    })
}));

const StyledBoxTop = styled(StyledBox)(({theme, locked}) => ({
    borderTopLeftRadius: theme.spacing(2),
    borderTopWidth: locked ? 3 : 0,
}));

const StyledBoxBottom = styled(StyledBox)(({theme, locked}) => ({
    borderBottomLeftRadius: theme.spacing(2),
    borderBottomWidth: locked ? 3 : 0,
}));

const StyledLockIcon = styled(Box)(({theme, slidersLocked}) => ({
    color: grey[500],
    transitionProperty: 'color',
    maxWidth: '100%',
    height: 24,
    transitionDuration: theme.transitions.duration.shortest,
    margin: theme.spacing(1, 0),
    '&:hover': {
        color: grey[300],
    },
    ...(slidersLocked && {
        margin: theme.spacing(0),
    })
}));

export default function SliderLockButton() {

    const slidersLocked = useImageStore((state) => state.slidersLocked)
    const toggleSlidersLocked = useImageStore((state) => state.toggleSlidersLocked)

    return (
        <>
            <StyledBoxTop locked={slidersLocked ? 1 : 0} />
            <StyledLockIcon onClick={toggleSlidersLocked} as={slidersLocked ? LockIcon : LockOpenIcon} slidersLocked={slidersLocked ? 1 : 0} />
            <StyledBoxBottom locked={slidersLocked ? 1 : 0} />
        </>
    );
}


