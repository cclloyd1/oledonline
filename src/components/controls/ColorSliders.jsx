import React from 'react';
import {useDebouncedCallback} from 'use-debounce';
import {styled} from '@mui/material/styles';
import useImageStore from '../../store/image.js';
import LockIcon from '@mui/icons-material/Lock';
import LockOpenIcon from '@mui/icons-material/LockOpen';
import Box from '@mui/material/Box';
import grey from '@mui/material/colors/grey';
import Color from 'color';
import {Collapse} from '@mui/material';
import GreenSlider from './GreenSlider.jsx';
import BlueSlider from './BlueSlider.jsx';
import RedSlider from './RedSlider.jsx';


export const variants = {
    RED: 'red',
    GREEN: 'green',
    BLUE: 'blue',
    ALPHA: 'alpha',
    ALL: 'all',
};


const StyledColorSliders = styled(Box)(({theme}) => ({
    display: 'flex',
    paddingBottom: theme.spacing(2),
}));

const StyledLockBox = styled(Box)(({theme, mobile, locked}) => ({
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'right',
    flexDirection: 'column',
    flexGrow: 0,
    flexShrink: 0,
    flexBasis: 40,
    padding: theme.spacing(2, 0),
    ...(locked && {
        padding: theme.spacing(0),
        alignItems: 'center',
        justifyContent: 'center',
    }),
}));


const StyledSliderBox = styled(Box)(({theme}) => ({
    flexGrow: 1,
}));

const StyledLockBoxBase = styled(Box)(({theme, locked}) => ({
    width: theme.spacing(2.5),
    height: `calc(50% - 12px)`,
    borderColor: grey[500],
    borderStyle: 'solid',
    borderWidth: 0,
    borderLeftWidth: 3,
    position: 'relative',
    left: '25%',
    [theme.breakpoints.down('md')]: {
        left: '12.5%',
    },
    ...(!locked && {
        borderLeftColor: `${Color(grey[500]).fade(0.5)}`,
        borderTopColor: 'transparent',
        borderBottomColor: 'transparent',
    }),
}));

const StyledBoxTop = styled(StyledLockBoxBase)(({theme, locked}) => ({
    borderTopLeftRadius: theme.spacing(2),
    borderTopWidth: locked ? 3 : 0,
    ...(locked && {
        display: 'none',
    })
}));

const StyledBoxBottom = styled(StyledLockBoxBase)(({theme, locked}) => ({
    borderBottomLeftRadius: theme.spacing(2),
    borderBottomWidth: locked ? 3 : 0,
    ...(locked && {
        display: 'none',
    })
}));

const StyledLockIcon = styled(Box)(({theme, locked}) => ({
    color: grey[500],
    transitionProperty: 'color',
    maxWidth: '100%',
    height: 24,
    transitionDuration: theme.transitions.duration.shortest,
    margin: theme.spacing(1, 0),
    '&:hover': {
        color: grey[300],
    },
    ...(locked && {
        margin: theme.spacing(0),
    })
}));

export default function ColorSliders({sx}) {
    const loaded = useImageStore((state) => state.loaded);
    const liveUpdate = useImageStore((state) => state.liveUpdate);
    const tRed = useImageStore((state) => state.tRed);
    const tGreen = useImageStore((state) => state.tGreen);
    const tBlue = useImageStore((state) => state.tBlue);
    const manipulating = useImageStore((state) => state.manipulating);
    const slidersLocked = useImageStore((state) => state.slidersLocked);
    const redToggled = useImageStore((state) => state.redToggled);
    const wasm = useImageStore((state) => state.wasm);

    const setManipulating = useImageStore((state) => state.setManipulating);
    const toggleSlidersLocked = useImageStore((state) => state.toggleSlidersLocked);
    const runOledify = useImageStore((state) => state.runOledify);
    const runRedify = useImageStore((state) => state.runRedify);

    const callManipulate = useDebouncedCallback(() => {
        if (!manipulating && loaded && liveUpdate && wasm) {
            setManipulating(true);
            if (redToggled) runRedify();
            else runOledify();
        }
    }, 250);

    // Split up call function to avoid unnecessary calls when tGreen/tBlue get updated when sliders are locked
    React.useEffect(() => {
        callManipulate()
    }, [tRed, tGreen, tBlue, callManipulate, slidersLocked, wasm]);


    return (
        <StyledColorSliders sx={sx}>
            <StyledLockBox locked={slidersLocked ? 1 : 0}>
                <StyledBoxTop locked={slidersLocked ? 1 : 0} />
                <StyledLockIcon onClick={toggleSlidersLocked} as={slidersLocked ? LockIcon : LockOpenIcon} locked={slidersLocked ? 1 : 0} />
                <StyledBoxBottom locked={slidersLocked ? 1 : 0} />
            </StyledLockBox>

            <StyledSliderBox xs={10} lg={11}>
                <RedSlider onManipulate={callManipulate}/>
                <Collapse in={!slidersLocked} timeout={250}>
                    <GreenSlider onManipulate={callManipulate}/>
                    <BlueSlider onManipulate={callManipulate}/>
                </Collapse>

            </StyledSliderBox>
        </StyledColorSliders>
    );
}
