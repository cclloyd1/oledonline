import React from 'react';
import ColorSlider from './ColorSlider.jsx';
import useImageStore from '../../store/image.js';
import {variants} from './ColorSliders.jsx';


export default function RedSlider({onManipulate}) {
    const loaded = useImageStore((state) => state.loaded);
    const tRed = useImageStore((state) => state.tRed);
    const setRed = useImageStore((state) => state.setRed);

    const handleColorChange = (color, value) => setRed(value);

    return (
        <ColorSlider
            type={variants.RED}
            value={tRed}
            onChange={handleColorChange}
            disabled={!loaded}
        />
    );
}
