import {TextField} from '@mui/material';
import React, {useEffect, useState} from 'react';
import {styled} from '@mui/material/styles';
import Slider from '@mui/material/Slider';
import {blue, green, grey, red} from '@mui/material/colors';
import PropTypes from 'prop-types';
import {variants} from './ColorSliders.jsx';
import useAppStore from '../../store/app.js';
import {useDebouncedCallback} from 'use-debounce';
import Box from '@mui/material/Box';
import useImageStore from '../../store/image.js';


const getVariantColor = (variant) => {
    switch(variant) {
        case variants.RED: return red[800];
        case variants.GREEN: return green[800];
        case variants.BLUE: return blue[800];
        default: return grey[500];
    }
}

const StyledColorSlider = styled(Box)(({theme}) => ({
    display: 'flex',
    alignItems: 'center',
    height: 48,
    boxSizing: 'border-box',
}));

const StyledColorLabelBox = styled(Box)(({theme}) => ({
    flexShrink: 0,
    flexGrow: 0,
    flexBasis: 40,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
}));

const StyledColorLabel = styled('span')(({theme, type, locked}) => ({
    textAlign: 'center',
    userSelect: 'none',
    display: 'inline-block',
    color: grey[500],
    fontFamily: 'sans-serif',
    fontWeight: 'bold',
    fontSize: '1.5rem',
    lineHeight: '100%',
    transitionProperty: 'all',
    transitionDuration: '150ms',
    margin: 0,
    ...((type === variants.RED || type === variants.ALL) && {color: getVariantColor(variants.RED) }),
    ...(type === variants.GREEN && {color: getVariantColor(variants.GREEN) }),
    ...(type === variants.BLUE && {color: getVariantColor(variants.BLUE) }),
    ...(locked && {
        fontSize: '1rem',
    }),
}));

const StyledColorLabelAll = styled(StyledColorLabel)(({theme, type}) => ({
    fontSize: '1rem',
}));

const StyledLabelCollapse = styled('div')(({theme, locked}) => ({
    whiteSpace: 'nowrap',
    width: 26,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    transitionProperty: 'all',
    transitionDuration: '150ms',
    overflow: 'hidden',
    ...(!locked && {
        width: 0,
    }),
}));

const StyledSliderBox = styled(Box)(({theme}) => ({
    minWidth: 100,
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(1),
    flexGrow: 1,
    flexShrink: 1,
}));

const StyledInputBox = styled(Box)(({theme}) => ({
    flexBasis: 85,
    flexShrink: 0,
    flexGrow: 0,
    paddingRight: theme.spacing(1),
    [theme.breakpoints.down('md')]: {
        flexBasis: 60,
    },
}));







export default function ColorSlider(props) {
    const { type, value, onChange, onChangeCommitted, disabled } = props;

    const [label, setLabel] = useState('');
    const [sliderValue, setSliderValue] = useState(0);
    const [valueLocked, setValueLocked] = useState(false);

    const slidersLocked = useImageStore((state) => state.slidersLocked)
    const loaded = useImageStore((state) => state.loaded)
    const isMobile = useAppStore((state) => state.isMobile)

    const handleOnChange = useDebouncedCallback(() => {
        if (valueLocked) {
            setValueLocked(false);
            return;
        }
        onChange(type, sliderValue);
    }, 100);

    const handleSliderChange = (event, newValue) => setSliderValue(newValue);

    const handleSliderChangeComitted = (event, newValue) => {
        setValueLocked(true);
        setSliderValue(newValue);
        onChange(type, newValue);
    }

    const handleInputChange = (event) => {
        setValueLocked(true);
        setSliderValue(Number(event.target.value));
        onChange(type, event.target.value === '' ? '' : Number(event.target.value));
    };

    useEffect(() => {
        if (type) {
            switch(type) {
                case variants.RED:
                    setLabel('R');
                    break;
                case variants.GREEN:
                    setLabel('G');
                    break;
                case variants.BLUE:
                    setLabel('B');
                    break;
                case variants.ALPHA:
                    setLabel('A');
                    break;
                default:
                    setLabel('R');
                    break;
            }
        }
    }, [type]);

    useEffect(() => {
        handleOnChange();
    }, [sliderValue]);

    return (
        <StyledColorSlider>
            <StyledColorLabelBox>
                <StyledColorLabel type={type} locked={slidersLocked ? 1 : 0} sx={{mr: '-1px'}}>{label ?? 'R'}</StyledColorLabel>
                {type !== variants.GREEN && type !== variants.BLUE &&
                    <StyledLabelCollapse locked={slidersLocked ? 1 : 0} orientation={'horizontal'}>
                        <StyledColorLabelAll sx={{color: getVariantColor(variants.GREEN)}}>G</StyledColorLabelAll>
                        <StyledColorLabelAll sx={{color: getVariantColor(variants.BLUE)}}>B</StyledColorLabelAll>
                    </StyledLabelCollapse>
                }
            </StyledColorLabelBox>
            <StyledSliderBox>
                <Slider
                    defaultValue={0} step={1} min={0} max={255}
                    value={sliderValue}
                    valueLabelDisplay={'auto'}
                    onChange={handleSliderChange}
                    onChangeCommitted={handleSliderChangeComitted}
                    disabled={disabled}
                    color={'secondary'}
                />
            </StyledSliderBox>
            <StyledInputBox>
                <TextField
                    margin={'none'}
                    value={sliderValue}
                    onChange={handleInputChange}
                    disabled={disabled}
                    variant={'outlined'}
                    size={'small'}
                    fullWidth
                    type={'number'}
                    inputProps={{
                        step: 1,
                        min: 0,
                        max: 255,
                        type: 'number',
                        inputMode: 'decimal',
                        sx: {
                            '-moz-appearance': isMobile || !loaded ? 'textfield' : 'auto',
                            '&:-webkit-inner-spin-button': {
                                '-webkit-appearance': 'none',
                                margin: 0,
                            },
                        }
                    }}
                />
            </StyledInputBox>
        </StyledColorSlider>
    );
}

ColorSlider.defaultProps = {
    variant: 'alpha',
    value: 0,
    setValue: undefined,
    disabled: false,
};

ColorSlider.propTypes = {
    onChange: PropTypes.func,
    onChangeCommitted: PropTypes.func,
}
