import React from 'react';
import ColorSlider from './ColorSlider.jsx';
import useImageStore from '../../store/image.js';
import {variants} from './ColorSliders.jsx';


export default function GreenSlider({onManipulate}) {
    const loaded = useImageStore((state) => state.loaded);
    const tGreen = useImageStore((state) => state.tGreen);
    const setGreen = useImageStore((state) => state.setGreen);
    const slidersLocked = useImageStore((state) => state.slidersLocked);

    const handleColorChange = (color, value) => setGreen(value);

    React.useEffect(() => {
        if (!slidersLocked) onManipulate()
    }, [tGreen, slidersLocked]);


    return (
        <ColorSlider
            type={variants.GREEN}
            value={tGreen}
            onChange={handleColorChange}
            disabled={!loaded}
        />
    );
}
