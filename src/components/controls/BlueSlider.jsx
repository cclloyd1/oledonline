import React from 'react';
import ColorSlider from './ColorSlider.jsx';
import useImageStore from '../../store/image.js';
import {variants} from './ColorSliders.jsx';


export default function BlueSlider({onManipulate}) {
    const loaded = useImageStore((state) => state.loaded);
    const tBlue = useImageStore((state) => state.tBlue);
    const setBlue = useImageStore((state) => state.setBlue);
    const slidersLocked = useImageStore((state) => state.slidersLocked);

    const handleColorChange = (color, value) => setBlue(value);

    React.useEffect(() => {
        if (!slidersLocked) onManipulate()
    }, [tBlue, slidersLocked]);

    
    return (
        <ColorSlider
            type={variants.BLUE}
            value={tBlue}
            onChange={handleColorChange}
            disabled={!loaded}
        />
    );
}
