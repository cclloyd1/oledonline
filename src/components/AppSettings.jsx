import React from 'react';
import {styled} from '@mui/material/styles';
import {Box, Collapse, IconButton, MenuItem, Select, Switch, Typography} from '@mui/material';
import useAppStore from '../store/app.js';
import {toolbarHeight} from '../theme.js';
import Slider from '@mui/material/Slider';
import ArrowDropUpIcon from '@mui/icons-material/ArrowDropUp';
import ArrowDropDownIcon from '@mui/icons-material/ArrowDropDown';
import AppSettingRow from "./AppSettingRow.jsx";

const StyledBox = styled(Box)(({theme,}) => ({
    width: '100%',
    padding: '0.5rem',
    fontFamily: "'Coda', sans-serif",
    boxSizing: 'border-box',
    marginTop: theme.spacing(4),
    [theme.breakpoints.down('md')]: {
        marginBottom: toolbarHeight + 20,
        marginTop: theme.spacing(2),
    }
}));

const StyledTitle = styled(Typography)(({theme, mobile}) => ({
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
}));


const StyledDivider = styled(Box)`
  background: linear-gradient(90deg, rgba(255,255,255,0.25) 25%, rgba(255,255,255,0.25) 30%, rgba(255,255,255,0) 100%);
  height: 2px;
  width: 100%;
`

export default function AppSettings() {
    const settingsOpen = useAppStore((state) => state.settingsOpen)
    const liveUpdate = useAppStore((state) => state.liveUpdate)
    const isMobile = useAppStore((state) => state.isMobile)
    const outlineCanvas = useAppStore((state) => state.outlineCanvas)
    const rememberImage = useAppStore((state) => state.rememberImage)
    const outputType = useAppStore((state) => state.outputType)
    const backgroundBrightness = useAppStore((state) => state.backgroundBrightness)

    const toggleSettingsOpen = useAppStore((state) => state.toggleSettingsOpen)
    const toggleLiveUpdate = useAppStore((state) => state.toggleLiveUpdate)
    const toggleOutlineCanvas = useAppStore((state) => state.toggleOutlineCanvas)
    const toggleRememberImage = useAppStore((state) => state.toggleRememberImage)
    const setOutputType = useAppStore((state) => state.setOutputType)
    const setStatSize = useAppStore((state) => state.setStatSize)
    const statSize = useAppStore((state) => state.statSize)
    const setBackgroundBrightness = useAppStore((state) => state.setBackgroundBrightness)

    const handleBackgroundBrightnessChange = (event, value) => setBackgroundBrightness(value);

    const handleStatSizeChange = (event, value) => setStatSize(value);


    return (
        <StyledBox>
            <StyledTitle variant={'h5'} sx={{mb: 1, cursor: 'pointer'}} onClick={toggleSettingsOpen}>
                App Settings
                <IconButton>{settingsOpen ? <ArrowDropDownIcon/> : <ArrowDropUpIcon/>}</IconButton>
            </StyledTitle>
            <Collapse in={settingsOpen}>
                <StyledDivider/>

                <Box>
                    <AppSettingRow title={'Live Update'} info={'Apply updates while dragging color sliders'}>
                        <Switch checked={liveUpdate} onChange={toggleLiveUpdate} color={'secondary'} />
                    </AppSettingRow>

                    <AppSettingRow title={'Outline Canvas'} info={'Visually outline the working canvas'}>
                        <Switch checked={outlineCanvas} onChange={toggleOutlineCanvas} color={'secondary'} />
                    </AppSettingRow>

                    <AppSettingRow title={'Remember Image'} info={'Automatically load previous image when website loads (currently disabled)'} disabled>
                        <Switch disabled checked={rememberImage} onChange={toggleRememberImage} color={'secondary'} />
                    </AppSettingRow>

                    <AppSettingRow title={'Output Format'} info={'Format to save the image as.'}>
                        <Select value={outputType} onChange={(e) => setOutputType(e.target.value)} fullWidth size={'small'}>
                            <MenuItem value={'png'}>.png</MenuItem>
                            <MenuItem value={'webp'}>.webp</MenuItem>
                        </Select>
                    </AppSettingRow>

                    <AppSettingRow title={'App Brightness'} info={'Brightness of the app (default: 0)'}>
                        <Slider
                            valueLabelDisplay={'auto'}
                            value={backgroundBrightness}
                            onChange={handleBackgroundBrightnessChange}
                            step={1}
                            marks
                            min={0}
                            max={9}
                            color={'secondary'}
                        />
                    </AppSettingRow>

                    <AppSettingRow title={'Stat Size'} info={'Font size of the black percentage'}>
                        <Slider
                            valueLabelDisplay={'auto'}
                            value={statSize}
                            onChange={handleStatSizeChange}
                            step={null}
                            min={8}
                            max={16}
                            marks={[
                                {
                                    value: 8,
                                    label: '1',
                                },
                                {
                                    value: 10,
                                    label: '2',
                                },
                                {
                                    value: 12,
                                    label: '3',
                                },
                                {
                                    value: 16,
                                    label: '4',
                                },]}
                            color={'secondary'}
                        />
                    </AppSettingRow>
                </Box>

            </Collapse>

        </StyledBox>
    );
}

