import React from 'react';
import {styled} from '@mui/material/styles';
import Box from "@mui/material/Box";
import ImageIcon from "@mui/icons-material/Image";
import grey from "@mui/material/colors/grey";
import useAppStore from '../store/app.js';
import useImageStore from '../store/image.js';
import {drawerWidth, toolbarHeight} from '../theme.js';


const StyledBox = styled(Box)(({theme, loaded, mobile}) => ({
    display: loaded ? 'none' : 'flex',
    flexGrow: 1,
    flexShrink: 1,
    width: `calc(100vw - ${drawerWidth}px)`,
    height: `calc(100vh - ${toolbarHeight}px)`,
    alignItems: 'center',
    justifyContent: 'center',
    transitionDuration: '150ms',
    transitionProperty: 'background-color',
    ...(mobile && {
        width: '100vw',
        height: `calc(100vh - ${toolbarHeight}px)`,
    }),
}));



const StyledPlaceholderBox = styled(Box)(({theme}) => ({
    display: 'flex',
    height: 'auto',
    //height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
}));

const StyledImageIcon = styled(ImageIcon)(({theme}) => ({
    width: '50%',
    height: '50%',
    color: grey[800],
}));

const StyledImg = styled('img')(({theme, mobile}) => ({
    maxWidth: '100%',
    maxHeight: mobile ? '100vh' : '100%',

}));


export default function PlaceholderImage({ src, alt }) {
    const mobile = useAppStore(state => state.isMobile);
    const backgroundBrightness = useAppStore(state => state.backgroundBrightness);
    const loaded = useImageStore(state => state.loaded);

    return (
        <StyledBox
            loaded={loaded ? 1 : 0}
            mobile={mobile ? 1 : 0}
        >
            <StyledPlaceholderBox>
                <StyledImageIcon/>
            </StyledPlaceholderBox>
        </StyledBox>
    );
}

Image.defaultProps = {
    src: undefined,
    alt: '',
};
