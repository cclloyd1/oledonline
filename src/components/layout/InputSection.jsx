import React, {useEffect, useState} from 'react';
import {styled} from '@mui/material/styles';
import {Box, Button, Collapse, darken, lighten, TextField} from '@mui/material';
import InputBase from '@mui/material/InputBase';
import grey from '@mui/material/colors/grey';
import PublishIcon from '@mui/icons-material/Publish';
import useImageStore from '../../store/image.js';
import useAppStore from '../../store/app.js';


const StyledInputSection = styled(Box)(({theme, loaded, brightness}) => ({
    boxSizing: 'border-box',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: loaded ? 0 : '1rem',
    paddingRight: loaded ? 0 : '1rem',
    paddingTop: loaded ? 0 : '1rem',
    backgroundColor: lighten(grey[900], (brightness === 9 ? 10 : brightness) / 10),
}));

const StyledInputArea = styled(Box)(({theme}) => ({
    position: 'relative',
    height: 72,
    width: '100%',
}));

const StyledTextField = styled(TextField)(({theme, useurl, loaded}) => ({
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100%',
    height: 72,
    opacity: useurl ? 1 : 0,
    transitionProperty: 'opacity',
    transitionDuration: '250ms',
    pointerEvents: useurl ? 'inherit' : 'none',

}));

const StyledLabelNew = styled('label')(({theme, useurl, loaded, brightness}) => ({
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100%',
    height: 72,
    opacity: useurl ? 0 : 1,
    transitionProperty: 'all',
    transitionDuration: '250ms',
    pointerEvents: useurl ? 'none' : 'inherit',
    cursor: 'pointer',
    overflowX: 'hidden',
    display: 'flex',
    textAlign: 'center',
    borderStyle: 'solid',
    alignItems: 'center',
    boxSizing: 'border-box',
    justifyContent: 'center',
    fontFamily: "'Coda', sans-serif",
    borderRadius: '1rem',
    borderWidth: '3px',
    fontSize: '24pt',
    overflow: 'hidden',
    padding: '0.5rem',
    color: brightness > 5 ? '#444' : '#eee',
    borderColor: brightness > 5 ? grey[500] : grey[800],
    backgroundColor: brightness === 0 ? '#353535' : lighten('#353535', (brightness+1) / 10),
    '&:hover': {
        backgroundColor: brightness === 0 ? '#414141' : lighten('#414141', (brightness+1) / 10),
        borderColor: brightness === 0 ? '#505050' : brightness > 5 ? darken('#505050', (10-brightness-1) / 10) : lighten('#505050', (brightness+1) / 10),
    },
    ...(loaded && {
        padding: '0.35rem 1rem',
        borderRadius: 0,
        borderWidth: 0,
        fontSize: '10pt',
        backgroundColor: brightness === 0 ? '#252525' : lighten('#252525', (brightness+1) / 10),
        boxShadow: brightness > 5 ? 'inset 0 0 10px rgba(0,0,0,0.3)' : 'inset 0 0 10px rgba(255,255,255,0.1)',
    }),
}));

export default function InputSection() {
    const [currentName, setCurrentName] = useState(null);
    const [init, setInit] = useState(false);

    const isMobile = useAppStore((state) => state.isMobile);
    const useURL = useAppStore((state) => state.useURL);
    const toggleUseURL = useAppStore((state) => state.toggleUseURL);
    const toggleDrawer = useAppStore((state) => state.toggleDrawer);
    const inputFile = useImageStore((state) => state.inputFile);
    const setInputFile = useImageStore((state) => state.setInputFile);
    const inputURL = useImageStore((state) => state.inputURL);
    const setInputURL = useImageStore((state) => state.setInputURL);
    const loadWasm = useImageStore((state) => state.loadWasm)
    const loaded = useImageStore((state) => state.loaded)
    const clearImage = useImageStore((state) => state.clearImage);
    const loadImageURL = useImageStore((state) => state.loadImageURL);
    const originalDataURL = useImageStore((state) => state.originalDataURL);
    const rememberImage = useAppStore((state) => state.rememberImage)
    const backgroundBrightness = useAppStore((state) => state.backgroundBrightness)


    useEffect(() => {
        if (!rememberImage) return;
        console.log('load from defualt', !useURL, originalDataURL !== null, !loaded)
        if (!useURL && originalDataURL !== null && !loaded) {
            loadWasm().then().catch(e => clearImage());
        }
    }, [originalDataURL, useURL, loaded, rememberImage])


    useEffect(() => {
        if (inputFile) {
            if (currentName !== inputFile.name) {
                loadWasm().then().catch(e => clearImage());
                setCurrentName(inputFile.name);
            }
        }
        else if (!inputFile && inputURL !== null) {
            try {
                if (!init) {
                    if (inputURL.length > 0) {
                        setInit(true);
                        handleFetchImage();
                    }
                    else {
                        setInit(true)
                    }
                }


            } catch (error) {
                console.error('Error loading image from existing url.', error);
                clearImage()
            }
        }
        else if (!inputFile && !inputURL) {
            setCurrentName(null);
        }
    }, [inputFile, inputFile?.name, inputURL])

    const handleFileChange = event => {
        if (event.target.files[0]) {
            try {
                let file = event.target.files[0];
                setInputFile(event.target.files[0]);
                if (isMobile) toggleDrawer();
                console.log('Loaded file', event.target.files[0]);
            }
            catch(err) {
                console.error('Error loading image file', err);
                setCurrentName(null);
                clearImage(null);
            }
        }
        else {
            setInputFile(null);
        }
    };

    const handleURLChange = (e) => setInputURL(e.target.value);

    const handleFetchImage = () => {
        loadImageURL();
        if (isMobile) toggleDrawer();
    }



    return (
        <StyledInputSection loaded={loaded ? 1 : 0} brightness={backgroundBrightness}>
            <InputBase
                sx={{display: 'none'}}
                id={'image'}
                type={'file'}
                onChange={handleFileChange}
                inputProps={{
                    accept: 'image/png, image/jpeg, image/webp',
                }}
            />
            <StyledInputArea>
                <StyledTextField
                    sx={{my: loaded ? 0 : 1, borderBottomWidth: 0,}}
                    id={'image-url'}
                    onChange={handleURLChange}
                    placeholder={'http://example.com/image.jpg'}
                    fullWidth
                    variant={'filled'}
                    label={'Image URL'}
                    color={'secondary'}
                    useurl={useURL ? 1 : 0}
                    loaded={loaded ? 1 : 0}
                    disabled={loaded}
                    value={inputURL}
                    InputProps={{
                        sx: {
                            transitionProperty: 'all',
                            transitionDuration: '150ms',
                            borderTopLeftRadius: loaded ? 0 : 4,
                            borderTopRightRadius: loaded ? 0 : 4,
                            height: loaded ? '100%' : 'auto',
                            '&:before': {
                                borderBottomWidth: 0,
                            },
                            '&:hover': {
                                borderBottomWidth: '0 !important',
                            },
                        }
                    }}
                />
                <StyledLabelNew loaded={loaded ? 1 : 0}  useurl={useURL ? 1 : 0} brightness={backgroundBrightness} htmlFor={'image'}>
                    {inputFile === null ? <>
                        <PublishIcon sx={{width: 50, height: 50}}/><span>Load Image</span>
                    </> : inputFile.name}
                </StyledLabelNew>
            </StyledInputArea>

            <Collapse in={!loaded}>
                <Button
                    variant={'contained'}
                    color={'secondary'}
                    onClick={() => handleFetchImage()}
                    sx={{mt: 2, mb: 2, mr: 2, display: useURL ? 'inline-flex' : 'none'}}
                    disabled={!(inputURL?.length > 0)}
                >Fetch Image</Button>
                <Button
                    variant={'contained'}
                    color={'primary'}
                    onClick={() => toggleUseURL()}
                    sx={{mt: 2, mb: 2}}
                >{useURL ? 'Load from file' : 'Grab URL'}</Button>
            </Collapse>

        </StyledInputSection>
    );
}

