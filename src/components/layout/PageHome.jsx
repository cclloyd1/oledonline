import {styled} from '@mui/material/styles';
import useAppStore from '../../store/app.js';
import TopBar from './TopBar.jsx';
import SideBar from './SideBar.jsx';
import CanvasStats from './CanvasStats.jsx';
import PlaceholderImage from '../PlaceholderImage.jsx';
import Canvas from '../Canvas.jsx';
import MobileImageControls from './mobile/MobileImageControls.jsx';
import React from 'react';
import {Box} from '@mui/material';
import {drawerWidth, toolbarHeight} from '../../theme.js';
import useImageStore from '../../store/image.js';
import grey from '@mui/material/colors/grey';


const Content = styled(Box)(({theme, brightness}) => ({
    width: `calc(100% - ${drawerWidth})`,
    height: `calc(100% - ${toolbarHeight}px)`,
    marginLeft: drawerWidth,
    marginTop: toolbarHeight,
    overflow: 'hidden',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
    position: 'relative',
    backgroundColor: brightness === 0 ? 'black' : brightness === 2 ? grey[(10-brightness-1)*100] : grey[(10-brightness)*100],
    objectFit: 'cover',
    [theme.breakpoints.down('md')]: {
        width: '100%',
        marginLeft: 0,
    },
}));

const CanvasWrapper = styled(Box)(({theme, loaded, mobile, controlheight}) => ({
    flexGrow: 1,
    flexShrink: 1,
    display: loaded ? 'flex' : 'none',
    height: '100%',
    maxHeight: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    objectFit: 'contain',
    boxSizing: 'border-box',
    transitionProperty: 'all',
    transitionDuration: '150ms',
    [theme.breakpoints.down('md')]: {
        maxHeight: `calc(100% - ${controlheight-4}px)`
    },
}));

// TODO: canvas not sized properly on mobile (locking and unlocking slider makes canvas stay small)

export default function PageHome() {
    const isMobile = useAppStore((state) => state.isMobile);
    const backgroundBrightness = useAppStore((state) => state.backgroundBrightness);
    const loaded = useImageStore((state) => state.loaded);
    const slidersLocked = useImageStore((state) => state.slidersLocked);

    return (
        <>
            <TopBar/>
            <SideBar/>
            <Content brightness={backgroundBrightness}>
                <PlaceholderImage/>
                <CanvasStats/>

                <CanvasWrapper mobile={isMobile ? 1 : 0} loaded={loaded ? 1 : 0} controlheight={slidersLocked ? 20+48+16 : 20+48*3+16}>
                    <Canvas/>
                </CanvasWrapper>
                {isMobile && loaded && <MobileImageControls/>}
            </Content>
        </>
    );
}