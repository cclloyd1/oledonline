import {Box, Button, Drawer, lighten} from '@mui/material';
import useImageStore from '../../store/image.js';
import useAppStore from '../../store/app.js';
import AppSettings from '../AppSettings.jsx';
import {drawerWidth, toolbarHeight} from '../../theme.js';
import InputSection from "./InputSection.jsx";
import DesktopImageControls from "./desktop/DesktopImageControls.jsx";
import grey from "@mui/material/colors/grey";


export default function SideBar() {
    const drawerOpen = useAppStore((state) => state.drawerOpen);
    const toggleDrawer = useAppStore((state) => state.toggleDrawer);
    const liveUpdate = useAppStore((state) => state.liveUpdate);
    const backgroundBrightness = useAppStore((state) => state.backgroundBrightness);
    const isMobile = useAppStore((state) => state.isMobile);
    const inputFile = useImageStore((state) => state.inputFile);
    const clearImage = useImageStore((state) => state.clearImage);
    const toggleRed = useImageStore((state) => state.toggleRed);
    const loaded = useImageStore((state) => state.loaded);
    const resetCanvas = useImageStore((state) => state.resetCanvas);

    return (
        <Drawer
            hideBackdrop
            open={isMobile ? drawerOpen : inputFile != null}
            onClose={toggleDrawer}
            anchor={'left'}
            variant={isMobile ? 'temporary' : 'permanent'}
            ModalProps={{sx: {mt: `${toolbarHeight}px`}, keepMounted: true}}
            PaperProps={{
                sx: {
                    width: drawerWidth,
                    boxSizing: 'border-box',
                    overflowX: 'hidden',
                    overflowY: 'auto',
                    mt: `${isMobile ? toolbarHeight : 0}px`,
                    display: 'flex',
                    flexDirection: 'column',
                    transitionProperty: 'all',
                    transitionDuration: '150ms',
                    backgroundColor: lighten('#121212', backgroundBrightness / 10),
                    color: backgroundBrightness > 5 ? '#111' : '#eee',
            }}}
        >
            <InputSection sx={{mt: 2}}/>
            {!isMobile && <DesktopImageControls/>}

            <Button
                onClick={clearImage}
                variant={'contained'}
                color={'primary'}
                sx={{m: 1, mt: liveUpdate ? 3 : 1}}
            >Reset Image</Button>

            <Button
                onClick={toggleRed}
                variant={'contained'}
                color={'primary'}
                sx={{m: 1}}
                disabled={!loaded}
            >Toggle Black / Red</Button>

            <Button
                onClick={resetCanvas}
                variant={'contained'}
                color={'primary'}
                sx={{m: 1}}
                disabled={!loaded}
            >Reset Canvas</Button>

            <Box sx={{flexGrow: 1}}/>

            <AppSettings/>
        </Drawer>
    );
}