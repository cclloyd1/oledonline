import React from 'react';
import {styled} from '@mui/material/styles';
import useImageStore from "../../store/image.js";
import {Box} from "@mui/material";
import useAppStore from '../../store/app.js';


const StyledStats = styled(Box)(({theme, loaded, statsize}) => ({
    position: 'absolute',
    top: 1,
    right: 0,
    color: '#eee',
    fontSize: `${statsize}pt`,
    fontFamily: 'monospace',
    minWidth: 20,
    backgroundColor: 'rgba(0,0,0,.5)',
    textAlign: 'right',
    padding: '0 5px',
    transition: 'all',
    pointerEvents: 'none',
    userSelect: 'none',
    zIndex: 1000,
    display: loaded ? 'flex' : 'none',
    gap: '0.5rem',
    touchAction: 'none',
}));


export default function CanvasStats(props) {
    const statSize = useAppStore((state) => state.statSize);
    const black = useImageStore((state) => state.black);
    const width = useImageStore((state) => state.width);
    const height = useImageStore((state) => state.height);
    const loaded = useImageStore((state) => state.loaded);

    if (black === null) return;

    return (
        <StyledStats loaded={loaded ? 1 : 0} statsize={statSize}>
            <span>{black.toLocaleString()} / {(width * height).toLocaleString()}</span>
            <span>({(black / (width*height) * 100).toFixed(2)} %)</span>
        </StyledStats>  
    );
}

