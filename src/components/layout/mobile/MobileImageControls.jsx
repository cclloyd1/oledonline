import {Box, Button, Collapse} from '@mui/material';
import useAppStore from '../../../store/app.js';
import grey from '@mui/material/colors/grey';
import {styled} from '@mui/material/styles';
import useImageStore from '../../../store/image.js';
import ColorSliders from '../../controls/ColorSliders.jsx';
import {useEffect, useRef} from 'react';
import {useDebouncedCallback} from 'use-debounce';


const StyledImageControls = styled(Box)(({theme}) => ({
    flexGrow: 0,
    flexShrink: 0,
    width: '100%',
    display: 'block',
}));

const PullerSection = styled(Box)(({theme}) => ({
    display: 'block',
    width: '100%',
    height: 16,
    borderRadius: '16px 16px 0 0',
    borderTop: `1px solid ${grey[800]}`,
    backgroundColor: grey[900],
    position: 'relative',
    boxSizing: 'border-box',
    '&:before': {
        content: '""',
        position: 'absolute',
        top: 4,
        left: '50%',
        marginLeft: -30,
        width: 60,
        height: 6,
        borderRadius: 10,
        background: grey[800],
    }
}));


const ControlBox = styled(Box)`
  background-color: ${grey[900]};
  text-align: center;
  user-select: none;
`


export default function MobileImageControls() {
    const ref = useRef('div');

    const liveUpdate = useAppStore((state) => state.liveUpdate);
    const runOledify = useImageStore((state) => state.runOledify);
    const setMobileImageRef = useAppStore((state) => state.setMobileImageRef);
    const slidersLocked = useImageStore((state) => state.slidersLocked);

    const handleHeightChange = useDebouncedCallback(() => {
        setMobileImageRef(ref?.current?.clientHeight)
    }, 150);

    useEffect(() => {
        handleHeightChange()
    }, [slidersLocked])

    const container = window !== undefined ? () => window.document.body : undefined;

    return (
        <>
            <StyledImageControls ref={ref}>
                <PullerSection/>
                <ControlBox>
                    {!liveUpdate &&
                        <Button
                            onClick={runOledify}
                            variant={'contained'}
                            color={'primary'}
                        >OLEDify</Button>
                    }
                    <ColorSliders/>
                </ControlBox>
            </StyledImageControls>
        </>

    );
}