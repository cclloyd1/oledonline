import {AppBar, IconButton, lighten, Toolbar, Typography} from '@mui/material';
import MenuIcon from '@mui/icons-material/Menu';
import useAppStore from '../../store/app.js';
import HelpMenu from '../HelpMenu.jsx';
import {drawerWidth} from '../../theme.js';
import {styled} from '@mui/material/styles';
import {Download} from '@mui/icons-material';
import useImageStore from '../../store/image.js';
import {saveAs} from 'file-saver'

const GitlabIcon = styled('img')(({theme}) => ({
    width: 36,
    height: 36,
    margin: -2,
    padding: 0,
}));

const StyledLogo = styled('img')(({theme}) => ({
    width: 36,
    height: 36,
    marginRight: theme.spacing(1),
    marginLeft: theme.spacing(-1),
}));

export default function TopBar() {
    const toggleDrawer = useAppStore((state) => state.toggleDrawer);
    const isMobile = useAppStore((state) => state.isMobile);
    const outputType = useAppStore((state) => state.outputType);
    const backgroundBrightness = useAppStore((state) => state.backgroundBrightness);

    const loaded = useImageStore((state) => state.loaded);
    const canvas = useImageStore((state) => state.canvas);
    const inputFile = useImageStore((state) => state.inputFile);

    const getFilename = () => (`${inputFile?.name.split('.')[0]}-oledified_${new Date().toLocaleString()}.${outputType}`)

    const handleDownload = () => {
        if (!canvas?.current) return
        canvas.current.toBlob(function(blob) {
            saveAs(blob, getFilename(), {}, true);
        });
    }

    const handleToggleDrawer = () => {
        if (loaded) toggleDrawer();
    }

    return (
        <AppBar
            position={'fixed'}
            sx={{
                width: { sm: `calc(100% - ${drawerWidth}px)` },
                ml: { sm: `${drawerWidth}px` },
                userSelect: 'none',
                backgroundColor: lighten('#121212', backgroundBrightness / 10),
                color: backgroundBrightness > 5 ? '#111' : '#eee',
            }}
        >
            <Toolbar>
                {isMobile && <IconButton
                    size='large'
                    edge='start'
                    color='inherit'
                    aria-label='menu'
                    sx={{ mr: 2 }}
                    disabled={!loaded}
                    onClick={handleToggleDrawer}
                >
                    <MenuIcon />
                </IconButton>}

                <StyledLogo src={'/favicon.png'}/>
                <Typography variant='h6' component='div' sx={{ flexGrow: 1 }}>
                    OLEDOnline
                </Typography>

                <a id={'downloadButton'} target={'_blank'} href={'/'} style={{display: 'none'}}> </a>
                <IconButton size={'large'} onClick={handleDownload} disabled={!loaded}>
                    <Download/>
                </IconButton>


                <a href={'https://gitlab.com/cclloyd1/oledonline'} target={'_blank'}>
                    <IconButton>
                        <GitlabIcon src={backgroundBrightness > 5 ? 'gitlab-logo-700-dark.svg' : 'gitlab-logo-700.svg'}/>
                    </IconButton>
                </a>

                <HelpMenu/>

            </Toolbar>
        </AppBar>
    );
}