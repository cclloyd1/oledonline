import {Box, Button} from '@mui/material';
import useAppStore from '../../../store/app.js';
import useImageStore from '../../../store/image.js';
import ColorSliders from '../../controls/ColorSliders.jsx';


export default function DesktopImageControls() {
    const liveUpdate = useAppStore((state) => state.liveUpdate);
    const runOledify = useImageStore((state) => state.runOledify);
    const loaded = useImageStore((state) => state.loaded);

    return (
        <>
            <ColorSliders sx={{mt: 2, pl: 0.5, pr: 1}}/>
            {!liveUpdate &&
                <Button
                    onClick={runOledify}
                    variant={'contained'}
                    color={'primary'}
                    sx={{m: 1, mt: 3}}
                    disabled={!loaded}
                >OLEDify</Button>
            }
        </>

    );
}