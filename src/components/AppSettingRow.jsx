import React, {useState} from 'react';
import {styled} from '@mui/material/styles';
import {Box, Typography} from '@mui/material';
import useAppStore from '../store/app.js';
import Tooltip from '@mui/material/Tooltip';
import PropTypes from "prop-types";
import Slider from "@mui/material/Slider";

const StyledBox = styled(Box)(({theme, open, disabled}) => ({
    width: '100%',
    padding: theme.spacing(1),
    boxSizing: 'border-box',
    position: 'relative',
    '&:after': {
        pointerEvents: 'none',
        zIndex: -1,
        content: '""',
        position: 'absolute',
        top: theme.spacing(1),
        bottom: theme.spacing(1),
        left: theme.spacing(1),
        right: theme.spacing(1),
        transitionProperty: 'opacity',
        transitionDuration: '250ms',
        opacity: open ? 1 : 0,
        background: `linear-gradient(0deg, rgba(255,255,255,0.1) 0%, rgba(255,255,255,0) 95%, rgba(255,255,255,0) 100%)`,
    }

}));

const SettingBox = styled(Box)(({theme,}) => ({
    display: 'flex',

}));

const HalfBox = styled(Box)(({theme, mobile}) => ({
    flexBasis: '50%',
    boxSizing: 'border-box',
    display: 'flex',
    alignItems: 'center',
}));

const LeftHalf = styled(HalfBox)(({theme, mobile, disabled}) => ({
    paddingLeft: theme.spacing(1),
    cursor: 'help',
    ...(mobile && {
        cursor: disabled ? 'default' : 'pointer',
    }),
}));

const RightHalf = styled(HalfBox)(({theme, extra, disabled}) => ({
    paddingRight: theme.spacing(extra ? 2 : 1),
    paddingLeft: theme.spacing(extra ? 1 : 0),
    opacity: disabled ? 0.5 : 1,
}));


const InfoBox = styled(Box)(({theme, open}) => ({
    fontSize: '0.75rem',
    paddingRight: theme.spacing(0.5),
    paddingLeft: theme.spacing(1),
    paddingTop: theme.spacing(open ? 0.5 : 0),
    paddingBottom: theme.spacing(open ? 1 : 0),
    fontFamily: 'sans-serif',
    overflow: 'hidden',
    height: open ? '100%' : 0,
    transitionProperty: 'all',
    transitionDuration: '150ms',
    position: 'relative',
    '&:after': {
        'content': '""',
        height: 1,
        width: '100%',
        background: 'linear-gradient(90deg, rgba(255,255,255,0.25) 25%, rgba(255,255,255,0.25) 30%, rgba(255,255,255,0) 100%)',
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0,
    }
}));

const FilledTypography = styled(Typography)(({theme, disabled}) => ({
    flexGrow: 1,
    width: '100%',
    height: '100%',
    display: 'inline-block',
    lineHeight: '46px',
    userSelect: 'none',
    opacity: disabled ? 0.5 : 1,
}));



export default function AppSettingRow({title, info, children, disabled}) {
    const [infoOpen, setInfoOpen] = useState(false);
    const isMobile = useAppStore((state) => state.isMobile);

    const handleClick = () => {
        if (isMobile && !disabled) setInfoOpen(!infoOpen)
    }

    return (
        <StyledBox open={infoOpen ? 1 : 0}>
            <SettingBox>

                <LeftHalf mobile={isMobile ? 1 : 0} disabled={disabled ? 1 : 0} onClick={handleClick}>
                    {isMobile && <FilledTypography disabled={disabled ? 1 : 0}>{title}</FilledTypography>}
                    {!isMobile && <Tooltip title={info} enterDelay={1000} enterTouchDelay={0} leaveTouchDelay={5000} arrow>
                        <FilledTypography disabled={disabled ? 1 : 0}>{title}</FilledTypography>
                    </Tooltip>}
                </LeftHalf>

                <RightHalf extra={children.type === Slider ? 1 : 0} disabled={disabled ? 1 : 0}>
                    {children}
                </RightHalf>
            </SettingBox>

            <InfoBox open={infoOpen ? 1 : 0}>{info}</InfoBox>
        </StyledBox>
    );
}

AppSettingRow.defaultProps = {
    title: '',
    info: '',
    children: null,
    disabled: false,
}

AppSettingRow.propTypes = {
    title: PropTypes.string.isRequired,
    info: PropTypes.string.isRequired,
    children: PropTypes.node,
    disabled: PropTypes.bool,
}