import React, {useEffect, useRef} from 'react';
import useImageStore from '../store/image.js';
import {styled} from '@mui/material/styles';
import useAppStore from '../store/app.js';
import {toolbarHeight} from '../theme.js';
import grey from '@mui/material/colors/grey';
import {Box} from '@mui/material';


const CanvasWrapper = styled(Box)(({theme, loaded, brightness}) => ({
    maxWidth: '100vw',
    display: loaded ? 'flex' : 'none',
    justifyContent: 'center',
    alignItems: 'center',
    boxSizing: 'border-box',
    position: 'relative',
    transitionDuration: '150ms',
    transitionProperty: 'background-color',
    backgroundColor: brightness,
}));

const StyledCanvas = styled('canvas')(({theme, type, isMobile, outlineCanvas, controlDrawerOpen, slidersLocked}) => ({
    maxWidth: '100vw',
    transitionProperty: 'all',
    transitionDuration: '0.2s',
    transitionTimingFunction: 'ease-out',
    borderWidth: 0,
    borderColor: '#555',
    borderStyle: 'solid',
    marginBottom: 0,
    maxHeight: `calc(100vh - ${toolbarHeight}px)`,
    flexShrink: 1,
    ...(outlineCanvas && {
        borderWidth: 2,
        boxShadow: '0 0 50px rgba(255,255,255,0.25), 0 0 15px rgba(255,255,255,0.25)',
    }),
}));

const NewStyledCanvas = styled('canvas')(({theme, outlineCanvas, controlsHeight}) => ({
    display: 'flex',
    flexGrow: 1,
    flexShrink: 1,
    objectFit: 'cover',
    maxWidth: '100%',
    transitionProperty: 'all',
    transitionDuration: '0.2s',
    transitionTimingFunction: 'ease-out',
    borderWidth: 0,
    borderColor: '#555',
    borderStyle: 'solid',
    marginBottom: 0,
    maxHeight: `calc(100%)`,
    boxSizing: 'border-box',
    ...(outlineCanvas && {
        borderWidth: 2,
        boxShadow: '0 0 50px rgba(255,255,255,0.25), 0 0 15px rgba(255,255,255,0.25)',
    }),
}));


export default function Canvas(props) {
    const ref = useRef('canvas');

    const controlDrawerOpen = useAppStore((state) => state.controlDrawerOpen);
    const isMobile = useAppStore((state) => state.isMobile);
    const outlineCanvas = useAppStore((state) => state.outlineCanvas);
    const mobileImageRef = useAppStore((state) => state.mobileImageRef);
    const useURL = useAppStore((state) => state.useURL);

    const inputFile = useImageStore((state) => state.inputFile);
    const inputURL = useImageStore((state) => state.inputURL);
    const loaded = useImageStore((state) => state.loaded);
    const canvas = useImageStore((state) => state.canvas);
    const setCanvas = useImageStore((state) => state.setCanvas);
    const setImg = useImageStore((state) => state.setImg);
    const readBlack = useImageStore((state) => state.readBlack);
    const wasm = useImageStore((state) => state.wasm);
    const width = useImageStore((state) => state.width);
    const height = useImageStore((state) => state.height);
    const slidersLocked = useImageStore((state) => state.slidersLocked);
    const redToggled = useImageStore((state) => state.redToggled);
    const runRedify = useImageStore((state) => state.runRedify);
    const runOledify = useImageStore((state) => state.runOledify);
    const osc = useImageStore((state) => state.osc);
    const originalData = useImageStore((state) => state.originalData);
    const loadedWasm = useImageStore((state) => state.loadedWasm);
    const originalDataURL = useImageStore((state) => state.originalDataURL);
    const setOriginalDataURL = useImageStore((state) => state.setOriginalDataURL);
    const setLoaded = useImageStore((state) => state.setLoaded);
    const setOriginalData = useImageStore((state) => state.setOriginalData);
    const setCurrentData = useImageStore((state) => state.setCurrentData);

    useEffect(() => {
        if (useURL) return;
        if (inputFile) setCanvas(ref)
        if (inputFile !== null && canvas?.current && wasm !== null) {
            console.log('drawing image', inputFile)
            drawOriginalImage().then();
        }
    }, [inputFile, osc, canvas, wasm]);

    useEffect(() => {
        if (!useURL) return;
        if (inputURL) setCanvas(ref)
        if (inputURL.length > 0 && canvas?.current && wasm) {
            console.log('drawing image 2')
            drawOriginalImage().then();
        }
    }, [inputURL, osc, canvas, wasm]);


    useEffect(() => {
        setCanvas(ref)
        if (!useURL && inputFile === null && originalDataURL !== null && canvas?.current && wasm) {
            console.log('DO THE THING')
            drawOriginalImage().then();
            //setLoaded(true);
        }
    }, [useURL, inputFile, originalDataURL, canvas, wasm]);


    useEffect(() => {
        if (loaded && wasm) {
            if (redToggled) runRedify();
            else runOledify();
        }
    }, [loaded, redToggled, wasm])


    const drawOriginalImage = async () => {
        const newImg = new Image(width, height);
        newImg.crossOrigin = "anonymous";
        if (originalDataURL && !inputFile && !useURL && !originalData) {
            newImg.src = originalDataURL;
            newImg.onload = () => {
                console.log('Drawing original image from existing data...')
                const canvas1 = canvas.current;
                canvas1.width = width;
                canvas1.height = height;
                const ctx = canvas1.getContext('2d');
                console.log('newImg', newImg);
                ctx.drawImage(newImg, 0, 0);
                readBlack().then();
                setImg(newImg);
                setLoaded(true);
                if (originalData === null) {
                    setOriginalData(ctx.getImageData(0, 0, newImg.width, newImg.height))
                    setCurrentData(ctx.getImageData(0, 0, newImg.width, newImg.height))
                }
            }
        }
        else {
            console.log('Drawing original image...')
            const canvas1 = canvas.current;
            canvas1.width = width;
            canvas1.height = height;
            const ctx = canvas1.getContext('2d');
            ctx.drawImage(newImg, 0, 0);
            readBlack().then();
            setImg(newImg);
            // if (originalDataURL === null) {
            //     setOriginalDataURL(canvas1.toDataURL('image/png'))
            // }
            if (originalData === null) {
                setOriginalData(ctx.getImageData(0, 0, newImg.width, newImg.height))
                setCurrentData(ctx.getImageData(0, 0, newImg.width, newImg.height))
            }
        }
        setImg(newImg);
    }

    return (
        <NewStyledCanvas
            ref={ref}
            controlDrawerOpen={controlDrawerOpen ? 1 : 0}
            isMobile={isMobile ? 1 : 0}
            outlineCanvas={outlineCanvas ? 1 : 0}
            slidersLocked={slidersLocked ? 1 : 0}
            controlsHeight={mobileImageRef}
        />
    )

    return(
        <>
            <CanvasWrapper
                loaded={loaded ? 1 : 0}
                brightness={backgroundBrightness === 0 ? 'black' : grey[(10-backgroundBrightness)*100]}
            >
                <StyledCanvas
                    ref={ref}
                    controlDrawerOpen={controlDrawerOpen ? 1 : 0}
                    isMobile={isMobile ? 1 : 0}
                    outlineCanvas={outlineCanvas ? 1 : 0}
                    slidersLocked={slidersLocked ? 1 : 0}
                />
            </CanvasWrapper>
        </>
    )
}
