import React from 'react';
import Dialog from "@mui/material/Dialog";
import DialogTitle from "@mui/material/DialogTitle";
import DialogContent from '@mui/material/DialogContent';
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import DialogActions from "@mui/material/DialogActions";
import {styled} from "@mui/material/styles";


const StyledTitle = styled(DialogTitle)(({theme, locked}) => ({
    fontSize: 28,
}));

export default function HelpDialog({ open, onClose }) {

    return (
        <Dialog open={open} onClose={onClose} scroll="body">
            <StyledTitle id={'dialog-title'}>
                How to use
            </StyledTitle>
            <DialogContent dividers>
                <Typography variant={'body1'} gutterBottom paragraph>To start, load an image or fetch an image from a URL.</Typography>
                <Typography variant={'body1'} gutterBottom paragraph>After choosing an image, drag the RGB slider(s) to a value greater than 0.  For each pixel, if the R, G, and B
                    values are less than the threshold, it will set that pixel to pure black.</Typography>
                <Typography variant={'body1'} gutterBottom paragraph>Once you set your threshold, the image will begin processing after a short delay and update in the canvas.  You can see the black percentage in the top right.</Typography>
                <Typography variant={'body1'}>Once you are satisfied with the image in the canvas, you can click the download button in the top bar to save the image.</Typography>
            </DialogContent>
            <DialogActions>
                <Button onClick={onClose} color={'primary'} variant={'contained'}>Close</Button>
            </DialogActions>
        </Dialog>
    );
}

HelpDialog.defaultProps = {
    open: false,
};
