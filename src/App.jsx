import './App.css'
import PageHome from './components/layout/PageHome.jsx';
import {ThemeProvider} from '@mui/material';
import {theme, lightTheme} from './theme.js';
import useAppStore from "./store/app.js";


function App() {
    const backgroundBrightness = useAppStore((state) => state.backgroundBrightness);

    return (
        <>
            <ThemeProvider theme={backgroundBrightness > 5 ? lightTheme : theme}>
                <PageHome/>
            </ThemeProvider>
        </>
    )
}

export default App
