import {createTheme} from '@mui/material';
import grey from '@mui/material/colors/grey.js';
import {blueGrey} from "@mui/material/colors";


export const toolbarHeight = 56;
export const drawerWidth = window.innerWidth < 768 ? '100vw' : 360;
const baseTheme = {
    palette: {
        mode: 'dark',
        primary: {
            main: blueGrey[800],
        },
        secondary: {
            main: blueGrey[600],
        }
    },
    mixins: {
        toolbar: {
            minHeight: toolbarHeight,
        }
    },
    components: {
        MuiPaper: {
            variants: [{
                props: { variant: 'dock' },
                style: {
                    backgroundColor: grey[900],
                    borderRadius: '20px 20px 0 0',
                    margin: '0 0',
                },
            }],
        },
        MuiBackdrop: {
            variants: [{
                props: { variant: 'hidden' },
                style: {
                    userSelect: 'none',
                },
            }],
        },
    }
}
export const theme = createTheme(baseTheme);

export const lightTheme = createTheme({
    ...baseTheme,
    palette: {
        ...baseTheme.palette,
        mode: 'light',
        primary: {
            main: blueGrey[600],
        },
        secondary: {
            main: blueGrey[400],
        }
    }
});