# OLED Online

[![pipeline status](https://gitlab.com/cclloyd1/oledonline/badges/master/pipeline.svg)](https://gitlab.com/cclloyd1/oledonline/-/commits/master)
 
---

This is a neat web app that lets you upload an image and turn it into an OLED wallpaper, all in your browser! Works offline and is 100% client-side. Also works on mobile.

You can find the hosted version on GitLab Pages at [https://cclloyd1.gitlab.io/oledonline/](https://cclloyd1.gitlab.io/oledonline/)

## Usage

There is a help panel in the app that explains how to use it, but it is repeated here.

- Load an image using the file upload
- Set the threshold for RGB to something >0.  
- Click manipulate
- See/Save new image in the preview panel.

Any pixel who's R, G, and B values are less than all thresholds will be set to pure black (#000000)

## Development

### Requirements

- node >=12.12.0

To help develop this app, simply clone the repo, go into the directory, and run `npm run start`.  To test a build run `npm run build`

### CI/CD

There is included a CI/CD pipeline file.  To properly load your app if you want to deploy to pages, you must set the PUBLIC_URL environment variable in your CI/CD settings to the full URI of your pages location. (eg. https://cclloyd1.gitlab.io/oledonline/)

## Technologies used

- [React](https://github.com/facebook/react)
- [Material-UI](https://material-ui.com/) - UI framework
- [Jimp](https://github.com/oliver-moran/jimp) - image manipulation
- [react-toast-notifications](https://github.com/jossmac/react-toast-notifications) - User feedback