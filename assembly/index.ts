


export function myExportedFunctionThatTakesAString(value: string): string {
  return "AsBindNew: " + value;
}

export function countBlack(width: i32, height: i32): u32 {
    let black: u32 = 0;
    for (let i = 0; i < width * height * 4; i += 4 /*rgba*/) {
        const r = load<u8>(i);
        const g = load<u8>(i + 1);
        const b = load<u8>(i + 2);
        if (r == 0 && b == 0 && g == 0) black++;
    }
    return black;
}

export function countRed(width: i32, height: i32): u32 {
    let black: u32 = 0;
    for (let i = 0; i < width * height * 4; i += 4 /*rgba*/) {
        const r = load<u8>(i);
        const g = load<u8>(i + 1);
        const b = load<u8>(i + 2);
        if (r == 255 && b == 0 && g == 0) black++;
    }
    return black;
}



export function oledify(width: i32, height: i32, rThreshold: u8 = 0, gThreshold: u8 = 0, bThreshold: u8 = 0): void {
    for (let i = 0; i < width * height * 4; i += 4 /*rgba*/) {
        const r = load<u8>(i);
        const g = load<u8>(i + 1);
        const b = load<u8>(i + 2);
        if (r <= rThreshold && g <= gThreshold && b <= bThreshold) {
            store<u8>(i, 0);
            store<u8>(i+1, 0);
            store<u8>(i+2, 0);
        }
    }
}


export function redify(width: i32, height: i32, rThreshold: u8 = 0, gThreshold: u8 = 0, bThreshold: u8 = 0): void {
    for (let i = 0; i < width * height * 4; i += 4 /*rgba*/) {
        const r = load<u8>(i);
        const g = load<u8>(i + 1);
        const b = load<u8>(i + 2);
        if (r <= rThreshold && g <= gThreshold && b <= bThreshold) {
            store<u8>(i, 255);
            store<u8>(i+1, 0);
            store<u8>(i+2, 0);
        }
    }
}



export function convertToGrayscaleOld(width: i32, height: i32): void {
    for (let i = 0; i < width * height * 4; i += 4 /*rgba*/) {
        const r = load<u8>(i);
        const g = load<u8>(i + 1);
        const b = load<u8>(i + 2);

        const gray = u8(r * 0.2126 + g * 0.7152 + b * 0.0722);

        store<u8>(i, gray);
        store<u8>(i + 1, gray);
        store<u8>(i + 2, gray);
    }

}


